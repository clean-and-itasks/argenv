#### 1.0.3

- Fix: remove dependency on `base` to avoid circular dependency in `base-compiler`.

#### 1.0.2

- Chore: Allow base-rts ^3.0, `base-stdenv` ^3.0 and base ^2.0 to support compiler ^3.0.

#### 1.0.1

- Chore: allow `base-stdenv` 2.0.

## 1.0.0

- First tagged version.
