# argenv

ArgEnv is a small library to access environment variables and command line
arguments. It is deprecated in favour of `System.Environment` and
`System.CommandLine` in [platform](https://clean-lang.org/pkg/platform).

This is a delayed mirror of the [upstream][] version and is only used to
publish the package. Periodically changes from upstream are released in a new
version here.

## Maintainer & license

This project is maintained by [TOP Software][].

For license details, see the [LICENSE](/LICENSE) file.

[TOP Software]: https://top-software.nl
[upstream]: https://gitlab.science.ru.nl/clean-and-itasks/clean-libraries
